import java.io.File;
import java.io.RandomAccessFile;
import java.util.Scanner;

public class Main
{
    private int intputInt(String message)
    {
        Scanner reader = new Scanner(System.in);
        int num = 0;
        boolean isCorrect;
        do
        {
            System.out.print(message);
            isCorrect = reader.hasNextInt();
            if (!isCorrect)
                reader.nextLine();
            else
                num = reader.nextInt();
        }
        while (!isCorrect || num <= 0);
        return num;
    }

    private String intputAny(String message)
    {
        Scanner reader = new Scanner(System.in);
        System.out.print(message);
        return reader.nextLine();
    }

    private double intputDouble(String message)
    {
        Scanner reader = new Scanner(System.in);
        boolean isCorrect;
        do
        {
            System.out.print(message);
            isCorrect = reader.hasNextDouble();
            if (!isCorrect)
                reader.nextLine();
        }
        while (!isCorrect);
        return reader.nextDouble();
    }

    private void createFile()
    {
        File f = new File("AleatorioEmple.dat");

        String userResponse = "yes";
        if (f.exists())
        {
            do
            {
                userResponse = intputAny("Old file will be deleted, are you sure you want to create a new file? yes/no: ");
                if (userResponse.equalsIgnoreCase("yes"))
                    System.out.println("Old File deleted: " + f.delete());
            }
            while (!userResponse.equalsIgnoreCase("yes") && !userResponse.equalsIgnoreCase("no"));
        }
        if (userResponse.equalsIgnoreCase("yes"))
        {
            try
            {
                RandomAccessFile file = new RandomAccessFile(f, "rw");

                String[] lastNames = {"FERNANDEZ", "GIL", "LOPEZ", "RAMOS", "SEVILLA", "CASILLA", "REY"};
                int[] dep = {10, 20, 10, 10, 30, 30, 20};
                Double[] salary = {1000.45, 2400.60, 3000.0, 1500.56, 2200.0, 1435.87, 2000.0};

                StringBuffer buffer;

                for (int i = 0; i < lastNames.length; i++)
                {
                    file.writeInt(i + 1);
                    buffer = new StringBuffer(lastNames[i]);
                    buffer.setLength(10);
                    file.writeChars(buffer.toString());
                    file.writeInt(dep[i]);
                    file.writeDouble(salary[i]);
                }
                file.close();
                System.out.println("File successfully created!");
            }
            catch (Exception e)
            {
                System.out.println(e.toString());
            }
        }
    }

    private void readFile()
    {
        File f = new File("AleatorioEmple.dat");

        try
        {
            RandomAccessFile file = new RandomAccessFile(f, "r");
            int id, dep, position = 0;
            double salary;
            char[] lastName = new char[10];

            while (file.getFilePointer() < file.length())
            {
                file.seek(position);
                id = file.readInt();
                if (id > 0)
                {
                    for (int i = 0; i < lastName.length; i++)
                        lastName[i] = file.readChar();

                    String lastNameStr = new String(lastName);
                    dep = file.readInt();
                    salary = file.readDouble();

                    System.out.printf("ID: %s, Last Name: %s, Department: %d, Salary: %.2f %n", id, lastNameStr.trim(), dep, salary);
                }
                position = position + 36;
            }
            file.close();
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
    }

    private String readFile(int id, boolean showLastName, boolean showDepartment, boolean showSalary)
    {
        File f = new File("AleatorioEmple.dat");
        StringBuilder resultStr = new StringBuilder();
        try
        {
            RandomAccessFile file = new RandomAccessFile(f, "r");

            int dep, position = (id - 1) * 36;
            double salary;
            char[] lastName = new char[10];

            file.seek(position);
            if (position >= file.length() || position < file.length() && file.readInt() <= 0)
                System.out.printf("ID: %d, employee doesn't exist...", id);
            else
            {
                file.seek(position + 4);
                for (int i = 0; i < lastName.length; i++)
                    lastName[i] = file.readChar();

                String lastNameStr = new String(lastName);
                dep = file.readInt();
                salary = file.readDouble();

                resultStr.append("ID: ");
                resultStr.append(id);
                if (showLastName)
                {
                    resultStr.append(", Last Name: ");
                    resultStr.append(lastNameStr.trim());
                }
                if (showDepartment)
                {
                    resultStr.append(", Department: ");
                    resultStr.append(dep);
                }
                if (showSalary)
                {
                    resultStr.append(", Salary: ");
                    resultStr.append(salary);
                }
            }
            file.close();
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
        return resultStr.toString();
    }

    private void ex1(int id)
    {
        System.out.println(readFile(id, true, true, true));
    }

    private void ex2(int id, String lastName, int department, double salary)
    {
        File f = new File("AleatorioEmple.dat");
        try
        {
            RandomAccessFile file = new RandomAccessFile(f, "rw");
            int position = (id - 1) * 36;
            file.seek(position);

            if (position < file.length() && file.readInt() > 0)
                System.out.println("ID: " + id + " employee already exist...");
            else
            {
                file.seek(position);
                file.writeInt(id);
                StringBuilder limitedString = new StringBuilder(lastName);
                limitedString.setLength(10);
                file.writeChars(limitedString.toString());
                file.writeInt(department);
                file.writeDouble(salary);
            }
            file.close();
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
    }

    private void ex3(int id, double raiseMoney)
    {
        File f = new File("AleatorioEmple.dat");
        try
        {
            RandomAccessFile file = new RandomAccessFile(f, "rw");

            int position = (id - 1) * 36;
            file.seek(position);
            if (position >= file.length() || position < file.length() && file.readInt() <= 0)
                System.out.printf("ID: %d, employee doesn't exist...", id);
            else
            {
                System.out.println("[OLD]" + readFile(id, true, false, true));

                position += 28;
                file.seek(position);
                raiseMoney += file.readDouble();
                file.seek(position);
                file.writeDouble(raiseMoney);
                file.close();

                System.out.println("[NEW]" + readFile(id, true, false, true));
            }
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
    }

    private void ex4(int id)
    {
        File f = new File("AleatorioEmple.dat");
        try
        {
            RandomAccessFile file = new RandomAccessFile(f, "rw");

            int position = (id - 1) * 36;
            file.seek(position);
            if (position >= file.length() || position < file.length() && file.readInt() <= 0)
                System.out.printf("ID: %d, employee doesn't exist...", id);
            else
            {
                StringBuilder lastNameStr = new StringBuilder(String.valueOf(id));
                lastNameStr.setLength(10);
                file.seek(position);
                file.writeInt(-1);
                file.writeChars(lastNameStr.toString());
                file.writeInt(0);
                file.writeDouble(0);
            }
            file.close();
            System.out.println("Employee, ID: " + id + " was successfully deleted!");
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
    }

    private void ex5()
    {
        StringBuilder resultStr = new StringBuilder();
        File f = new File("AleatorioEmple.dat");
        try
        {
            RandomAccessFile file = new RandomAccessFile(f, "rw");

            int position = 0;
            char[] id = new char[10];
            while (position < file.length())
            {
                file.seek(position);
                if (file.readInt() == -1)
                {
                    resultStr.append("[ID]");
                    for (int i = 0; i < 10; i++)
                        id[i] = file.readChar();
                    resultStr.append(new String(id).trim());
                    resultStr.append(", ");
                }
                position += 36;
            }
            file.close();
            System.out.println("Eliminated: " + resultStr.toString());
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
    }

    private boolean getInputMenu()
    {
        switch (intputAny("\n\nSelect an option by [command] or number: ").toLowerCase())
        {
            case "1": case "semp": case "search": ex1(intputInt("Write the employee ID: "));
            intputAny("\n\nPress enter to continue...");
            break;
            case "2": case "nemp":
            case "add": ex2(intputInt("Write employee ID: "), intputAny("Write the employee last name: "), intputInt("Write the employee department: "), intputDouble("Write the employee salary: "));
                intputAny("\n\nPress enter to continue...");
                break;
            case "3": case "rsal":
            case "raise": ex3(intputInt("Write the employee ID: "), intputDouble("Write the extra salary for the employee: "));
                intputAny("\n\nPress enter to continue...");
                break;
            case "4": case "del": case "delete": ex4(intputInt("Write the employee ID: "));
            intputAny("\n\nPress enter to continue...");
            break;
            case "5": case "sdel": case "deleted": ex5();
            intputAny("\n\nPress enter to continue...");
            break;
            case "6": case "rfile": case "read": readFile();
            intputAny("\n\nPress enter to continue...");
            break;
            case "7": case "cfile": case "create": createFile();
            intputAny("\n\nPress enter to continue...");
            break;
            case "8": case "e": case "exit": return true;
        }
        return false;
    }

    private void drawMenu()
    {
        System.out.print("=============== Welcome to my Program ===============\n\n" +
                "1[semp, search]  Search for an employee.\n" +
                "2[nemp, add]     Add new employee.\n" +
                "3[rsal, raise]   Raise an employee salary.\n" +
                "4[del, delete]   Delete an employee by ID.\n" +
                "5[sdel, deleted] See deleted employees.\n" +
                "6[rfile, read]   Read file.\n" +
                "7[cfile, create] Create default file.\n" +
                "8[e, exit]       Exit");
    }

    private void startMenu()
    {
        boolean finished;
        do
        {
            drawMenu();
            finished = getInputMenu();
        }
        while (!finished);
        System.out.println("Ending Program! :D");
    }

    public static void main(String[] args)
    {
        Main program = new Main();
        program.startMenu();
    }
}
